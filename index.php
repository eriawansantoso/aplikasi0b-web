<?php
    $DB_NAME = "review";
    $DB_USER = "root";
    $DB_PASS = "";
    $DB_SERVER_LOC = "localhost";

    $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
    $sql = "SELECT p.id_posting, p.judul_post, k.nama_kategori, p.deskripsi, p.photos
            FROM posting p, kategori k 
            WHERE p.id_kategori = k.id_kategori";
    
    $result = mysqli_query($conn,$sql);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Review Electronic</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body class="bg-light">
    <div class="container">
        <div class="row justify-content-center mt-5">
            <h4>Data Postingan</h4>
            <table class="mt-3 table table-bordered">
                <tr>
                    <th>id_posting</th>
                    <th>judul_post</th>
                    <th>kategori</th>
                    <th>deskripsi</th>
                    <th>foto</th>
                    <th>opsi</th>
                </tr>
                <?php 
                    while($pst = mysqli_fetch_assoc($result)){
                ?>
                <tr>                 
                    <td><?php echo $pst['id_posting']; ?></td>
                    <td><?php echo $pst['judul_post']; ?></td>
                    <td><?php echo $pst['nama_kategori']; ?></td>
                    <td><?php echo $pst['deskripsi']; ?></td>
                    <td><img src="images/<?php echo $pst['photos']; ?>" style="width: 100px;" alt="Foto Postingan"></td>
                    <td><?php echo "<a href='proses-hapus.php?id_posting=".$pst['id_posting']."'>Hapus</a>"; ?></td>
                </tr> 
                <?php } ?>
            </table>
        </div>
    </div>
</body>
</html>