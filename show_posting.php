<?php

$DB_NAME = "review";
$DB_USER = "root";
$DB_PASS = "";
$DB_SERVER_LOC = "localhost";

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $conn = mysqli_connect($DB_SERVER_LOC, $DB_USER, $DB_PASS, $DB_NAME);
    // $judul_post = $_POST['judul_post'];
    $sql = "SELECT p.id_posting, p.judul_post, k.nama_kategori, p.deskripsi, 
            concat('http://192.168.43.104/aplikasi0b-web/images/',p.photos) as url
            from posting p, kategori k
            where p.id_kategori = k.id_kategori";

    $result = mysqli_query($conn, $sql);
    if (mysqli_num_rows($result) > 0) {
        header("Access-Control-Allow-Origin: *");
        header("Content-type: application/json; charset=UTF-8");

        $data_posting = array();
        while ($posting = mysqli_fetch_assoc($result)) {
            array_push($data_posting, $posting);
        }
        echo json_encode($data_posting);
    }
}
